const Joi = require('@hapi/joi')
Joi.objectId = require('joi-objectid')(Joi)


const contact = Joi.object({
    email: Joi.string().email()
    , phone: Joi.string().max(15)
})

const address = Joi.object({
    zipCode: Joi.string().max(15)
    , street: Joi.string().max(200)
    , district: Joi.string().max(100)
    , city: Joi.string().max(100)
    , state: Joi.object({
        name: Joi.string().max(100)
        , iso: Joi.string().max(5)
    })
    , country: Joi.object({
        name: Joi.string().max(100)
        , iso: Joi.string().max(5)
    })
})

const login = Joi.string().max(256)
const password = Joi.string().min(6)
const cpf = Joi.string().max(20)
const rg = Joi.string().max(20)

module.exports.create = Joi.object({
    login: login.required()
    , password: password.required()
    , cpf
    , rg
    , contact
    , address
    , admin: Joi.boolean()
    , platformId: Joi.objectId()
})

module.exports.update = Joi.object({
    login
    , password
    , cpf
    , rg
    , contact
    , address
    , admin: Joi.boolean()
    , platformId: Joi.objectId()
})


module.exports.signin = Joi.object({
    login: login.required()
    , password: password.required()
})