const { BaseRepository, MongoConnection } = require("will-core-lib/connectors/mongodb")
const { UserSchema } = require('./../Models/user')


class Repository extends BaseRepository {
    constructor() {
        super('users', UserSchema, new MongoConnection(process.env.MONGODB))
    }
}

module.exports = new Repository()