const { ResponseCreated, ResponseError, UnauthorizedException, UnprocessableEntityException } = require('will-core-lib/http')
const { CREATED_SUCCESS, SUCCESS } = require('will-core-lib/http/status-code')
const { AuthLambda: Auth } = require('will-internal-integrations')
const { JoiValidation } = require('will-core-lib/validators')
const { create: createSchema } = require('./../SchemaValidator/UserValidator')
const UserRepository = require('./../Repositories/UserRepository')

module.exports.handler = async({ body, headers }) => {
    try {
        const platformKey = headers['x-api-key']
        body = JSON.parse(body)

        await JoiValidation(createSchema, body)

        const founded = await UserRepository.findOne({
            login: body.login
            , platformKey
        })
        if (founded) throw new UnauthorizedException('User Already exists')

        Auth.key = platformKey

        body.platformKey = platformKey
        const user = await UserRepository.create(body)

        await Auth.createUser({
            login: body.login
            , password: body.password
            , custom: {
                userId: user._id,
                login: user.login
            }
        })

        if (Auth.getRuntimeStatusCode() !== SUCCESS && Auth.getResponseStatusCode() !== CREATED_SUCCESS) {
            throw new UnprocessableEntityException('Auth error')
        }

        user.dataAuth = Auth.getResponseBody()
        
        await UserRepository.save(user)

        const token = user.dataAuth.token

        return new ResponseCreated({
            userId: user._id
            , token
        })
    }
    catch (error) {
        return new ResponseError(error)
    }
}
