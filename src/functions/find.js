const { ResponseSuccess, ResponseError, NotFoundException, UnauthorizedException } = require('will-core-lib/http')
const UserRepository = require('./../Repositories/UserRepository')

module.exports.handler = async({ headers, pathParameters: { id } }) => {
    try {
        const platformKey = headers['x-api-key']
        id = await UserRepository.convertToObjectId(id)

        if (!platformKey) throw new UnauthorizedException('Key not provided')
    
        const user = await UserRepository.findOne({ 
            _id: id
            , platformKey
        })

        if(!user) {
            throw new NotFoundException('User not exists')
        }

        return new ResponseSuccess(user)
    }
    catch (error) {
        return new ResponseError(error)
    }
}
