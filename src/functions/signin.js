const { Response, ResponseSuccess, ResponseError, NotFoundException } = require('will-core-lib/http')
const { SUCCESS } = require('will-core-lib/http/status-code')
const { AuthLambda: Auth } = require('will-internal-integrations')
const { JoiValidation } = require('will-core-lib/validators')
const { signin: signinSchema } = require('./../SchemaValidator/UserValidator')
const UserRepository = require('./../Repositories/UserRepository')

module.exports.handler = async({ body, headers }) => {
    try {
        const platformKey = headers['x-api-key']
        body = JSON.parse(body)

        await JoiValidation(signinSchema, body)

        const user = await UserRepository.findOne({
            login: body.login
            , platformKey
        })
        if (!user) throw new NotFoundException('User not found')

        Auth.key = platformKey

        await Auth.signin({
            login: body.login
            , password: body.password
            , custom: {
                userId: user._id
                , login: user.login
            }
        })

        if (Auth.getRuntimeStatusCode() !== SUCCESS || Auth.getResponseStatusCode() !== SUCCESS) {
            return new Response(
                Auth.getResponseStatusCode(),
                Auth.getResponseBody()
            )
        }

        const token = Auth.getResponseBody().token

        return new ResponseSuccess({
            userId: user._id
            , token
        })
    }
    catch (error) {
        return new ResponseError(error)
    }
}
