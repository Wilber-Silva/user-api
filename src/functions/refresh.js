const { Response, ResponseSuccess, ResponseError, NotFoundException, UnauthorizedException } = require('will-core-lib/http')
const { SUCCESS } = require('will-core-lib/http/status-code')
const { bearerToken } = require('will-core-lib/helpers/extract')
const { AuthLambda: Auth } = require('will-internal-integrations')

module.exports.handler = async({ headers }) => {
    try {
        const platformKey = headers['x-api-key']
        const token = await bearerToken(headers.Authorization)

        Auth.key = platformKey

        await Auth.refreshAccess({ token })
        if (Auth.getRuntimeStatusCode() !== SUCCESS && Auth.getResponseStatusCode() !== SUCCESS) {
            return new Response(
                Auth.getResponseStatusCode(),
                Auth.getResponseBody()
            )
        }

        return new ResponseSuccess(Auth.getResponseBody())
    }
    catch (error) {
        return new ResponseError(error)
    }
}
