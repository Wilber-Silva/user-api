const { ResponseNotContent, ResponseError, NotFoundException, UnprocessableEntityException, UnauthorizedException } = require('will-core-lib/http')
const { NOT_CONTENT, SUCCESS } = require('will-core-lib/http/status-code')
const { AuthLambda: Auth } = require('will-internal-integrations')
const { JoiValidation } = require('will-core-lib/validators')
const { update: updateSchema } = require('./../SchemaValidator/UserValidator')
const UserRepository = require('./../Repositories/UserRepository')

module.exports.handler = async({ body, headers, pathParameters: { id } }) => {
    try {
        const platformKey = headers['x-api-key']
        id = await UserRepository.convertToObjectId(id)

        if (!platformKey) throw new UnauthorizedException('Key not provided')
        
        body = JSON.parse(body)

        await JoiValidation(updateSchema, body)

        const UserModel = await UserRepository.getModel()
        const user = await UserModel.findOne({ _id: id, platformKey }).select('+dataAuth')

        if(!user) {
            throw new NotFoundException('User not exists')
        }

        if (body.login || body.password){
            if (body.login !== user.login){
                const duplicate = await UserRepository.findOne({ login: body.login, platformKey })
               
                if (duplicate) {
                    throw new UnprocessableEntityException('Email already exists')
                }
            }
            Auth.key = platformKey
            await Auth.updateUser(
                user.dataAuth._id.toString()
                , {
                    login: body.login
                    , password: body.password
                }
            )
        }

        if (Auth.getRuntimeStatusCode() === SUCCESS && Auth.getResponseStatusCode() === NOT_CONTENT) {
            
            const { nMatched, nModified  } = await UserRepository.update(
                { _id: id, platformKey }
                , { $set: {...body} }
            )

            if ( nMatched > 0 || nModified > 0 ) {
                return new ResponseNotContent()
            }
        }

        throw new NotFoundException('User not found')
    }
    catch (error) {
        return new ResponseError(error)
    }
}
