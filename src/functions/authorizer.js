const { UnauthorizedException } = require('will-core-lib/http')
const { SUCCESS } = require('will-core-lib/http/status-code')
const { AuthLambda: Auth } = require('will-internal-integrations')
const { bearerToken } = require('will-core-lib/helpers/extract')


module.exports.handler = async ({ methodArn, headers }) => {
  try{
    Auth.key = headers['x-api-key']
    const token = await bearerToken(headers.Authorization)
    await Auth.validateAccess({ token })

    if (Auth.getRuntimeStatusCode() !== SUCCESS || Auth.getResponseStatusCode() !== SUCCESS) {
      console.log('Lambda Error', Auth.getResponseBody())
      throw new UnauthorizedException('Authentication fail')
    }

    const decoded = Auth.getResponseBody()
    const policy = this.generatePolicy(
      {
        principalId: decoded.custom.userId
        , Effect: 'Allow'
        , Resource: methodArn
      }
    )

    console.log('policy', policy)
    return policy
  }
  catch (error) {
    console.log('Unauthorized', error)
    return 'Unauthorized'
  }
}

module.exports.generatePolicy = ({ principalId, Effect, Resource }) => (
  {
    principalId
    , policyDocument: {
      Version: '2012-10-17'
      , Statement: [
        {
          Action: 'execute-api:Invoke'
          , Effect
          , Resource
        }
      ]
    }
  }
)
