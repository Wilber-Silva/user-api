
const mongoose = require('mongoose')

const contact = {
    email: { type: String }
    , phone: { type: String }
}

const address = {
    zipCode: { type: String }
    , street: { type: String }
    , district: { type: String }
    , city: { type: String }
    , state: {
        name: { type: String }
        , iso: { type: String }
    }
    , country: {
        name: { type: String }
        , iso: { type: String }
    }
}

module.exports.UserSchema = new mongoose.Schema({
    login: { type: String, require: true}
    , cpf: { type: String }
    , rg: { type: String }

    , contact
    , address

    , admin: { type: Boolean, default: false }
    
    , dataAuth: { type: Object, select: false }
    , platformKey: { type: String, select: false }

    , integrated: { Boolean: false }

    // Required in all schemas 
    , createdAt: { type: Date, default: Date.now }
    , updatedAt: { type: Date, default: null }
    , deletedAt: { type: Date, default: null }
})
